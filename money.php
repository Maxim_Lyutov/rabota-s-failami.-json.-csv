#!/usr/bin/env php
<?php
    
    if (count($argv) > 1)
    {
        if ($argv[1] == '--today')
        {
            if (!file_exists(‘money.csv’))
	        { 
                echo 'Не найдет файл money.csv.Скрипт прерван...'; 
	            exit ;
	         }
            $sum = 0;
            if (($handle = fopen('money.csv','r' ))!==FALSE  )
            {
                while (($data = fgetcsv($handle, 1000, ','))!== FALSE)
                {
                    if ( $data[0] ==  date('Y-m-d') )
                    {
                       $sum = $sum + $data[1];
                    }
                }
                echo "$data[0] расход за день: $sum  \n";
                fclose($handle);
            }
        }
        elseif(count($argv) > 2)
        {
            $result[] = date('Y-m-d');                  //date
            $result[] = $argv[1];                       //price
            $result[] = implode(array_slice(' ', $argv, 2)); //name product
            
            $handle = fopen('money.csv','a+' );
            fputcsv($handle, $result);
            fclose($handle);
            
            echo "Добавлена строка: $result[0], $result[1] ,$result[2] \n";
            
        }
        else
        {
            echo "Ошибка! Аргументы д.б. {цена} и {описание покупки}\n";
        }
    }
    else
    {
        echo "Ошибка! Аргументы не заданы. Укажите флаг --today или запустите скрипт с аргументами {цена} и {описание покупки}\n";
    }

?>
