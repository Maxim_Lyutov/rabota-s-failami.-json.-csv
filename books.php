<?php
    //Google Books API
	
	function getValue($arg) 
	{
        return (!empty($arg)) ? $arg : '';
    }
	
    if ( count($argv) > 1)
    {
        $query = implode(array_slice($argv, 1));
        $query = urlencode($query);
        $data_json = @file_get_contents("https://www.googleapis.com/books/v1/volumes?q=$query&maxResults=5");
        
        if ($data_json !== FALSE)
        {
            $data_array = json_decode($data_json, true); 
            switch (json_last_error()) 
            {
                case JSON_ERROR_NONE:
                    echo ' - Ошибок нет';
                break;
                case JSON_ERROR_DEPTH:
                    echo ' - Достигнута максимальная глубина стека';
                break;
                case JSON_ERROR_STATE_MISMATCH:
                    echo ' - Некорректные разряды или несоответствие режимов';
                break;
                case JSON_ERROR_CTRL_CHAR:
                    echo ' - Некорректный управляющий символ';
                break;
                case JSON_ERROR_SYNTAX:
                    echo ' - Синтаксическая ошибка, некорректный JSON';
                break;
                case JSON_ERROR_UTF8:
                    echo ' - Некорректные символы UTF-8, возможно неверно закодирован';
                break;
                default:
                    echo ' - Неизвестная ошибка';
                break;
            }

            $handle = fopen('books.csv','a+' );

            foreach ($data_array["items"] as $data )
            {   
			    $id = getValue($data["id"]);
				$title = getValue($data["volumeInfo"]["title"]);
				$authors = getValue($data["volumeInfo"]["authors"][0]);
				
                $tmp_str = "$id,$title,$authors";
                fputcsv($handle, explode(',', $tmp_str));
            }
            fclose($handle);
        }
        else
        {
           echo "Ошибка : по запросу нет данных...\n";
        }
    }
    else
    {
      echo "Ошибка : не верное кол-во параметров... \n";
    }
?>
